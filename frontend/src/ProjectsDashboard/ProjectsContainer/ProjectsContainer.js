import React, { useState, useEffect } from "react";
import axios from "axios";
import Project from "../Project/Project";
import "./ProjectsContainer.css";
import { BASE_URL } from "../../config";

function ProjectsContainer({ user }) {
  const [projects, setProjects] = useState([]);
  const [newProject, setNewProject] = useState("");

  const getProjects = async () => {
    const { data } = await axios.get(`${BASE_URL}/projects`, { headers: { authorization: user } });
    setProjects(data[0]);
  };

  const saveProject = async () => {
    await axios.post(`${BASE_URL}/projects`, { title: newProject }, { headers: { authorization: user } });
    setNewProject("");
  };

  useEffect(() => {
    getProjects();
  });

  return (
    <div className="projectDashboard">
      <div className="addProject">
        <input value={newProject} placeholder="Add Project" onChange={({ target }) => setNewProject(target.value)} />
        <span role="img" aria-label="save" onClick={() => saveProject()}>
          💾
        </span>
      </div>
      <div className="projectsContainer">
        {projects.map(({ id, title, tasks }) => (
          <Project projectId={id} title={title} tasks={tasks} user={user} key={`project${id}`} />
        ))}
      </div>
    </div>
  );
}

export default ProjectsContainer;
