import React, { useState } from "react";
import Task from "../Task/Task";
import "./Project.css";
import axios from "axios";
import { BASE_URL } from "../../config";

function Project({ projectId, title, tasks, user }) {
  const [projectTitle, setProjectTitle] = useState(title);
  const [newTask, setNewTask] = useState("");

  const saveTask = async () => {
    await axios.post(`${BASE_URL}/tasks`, { todo: newTask, projectId }, { headers: { authorization: user } });
    setNewTask("");
  };

  const saveProject = async () => {
    await axios.put(`${BASE_URL}/projects/${projectId}`, { title: projectTitle }, { headers: { authorization: user } });
  };
  const deleteProject = async () => {
    await axios.delete(`${BASE_URL}/projects/${projectId}`, { headers: { authorization: user } });
  };

  return (
    <div className="project">
      <div className="projectTitle">
        <input value={projectTitle} onChange={({ target }) => setProjectTitle(target.value)} />
        <span role="img" aria-label="save" onClick={() => saveProject()}>
          💾
        </span>
        <span role="img" aria-label="delete" onClick={() => deleteProject()}>
          🗑
        </span>
      </div>

      {tasks.map(({ id, todo, state }) => (
        <Task taskId={id} todo={todo} completed={state} projectId={projectId} user={user} key={`task${id}`} />
      ))}

      <div className="addTask">
        <input value={newTask} placeholder="Add Task" onChange={({ target }) => setNewTask(target.value)} />
        <span role="img" aria-label="save" onClick={() => saveTask()}>
          💾
        </span>
      </div>
    </div>
  );
}

export default Project;
