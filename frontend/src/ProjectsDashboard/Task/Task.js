import React, { useState } from "react";
import { BASE_URL } from "../../config";
import axios from "axios";

function Task({ todo, taskId, projectId, user, completed }) {
  const [taskTodo, setTodo] = useState(todo);

  const saveTask = async () => {
    await axios.put(`${BASE_URL}/tasks/${taskId}`, { todo: taskTodo, projectId }, { headers: { authorization: user } });
  };
  const deleteTask = async () => {
    await axios.delete(`${BASE_URL}/tasks/${taskId}`, { headers: { authorization: user } });
  };
  const completeTask = async () => {
    await axios.patch(`${BASE_URL}/tasks/${taskId}`, {}, { headers: { authorization: user } });
  };

  return (
    <div className="task">
      <input value={taskTodo} onChange={({ target }) => setTodo(target.value)} disabled={completed} />
      {!completed && (
        <span>
          <span role="img" aria-label="save" onClick={() => saveTask()}>
            💾
          </span>
          <span role="img" aria-label="delete" onClick={() => deleteTask()}>
            🗑
          </span>
          <span role="img" aria-label="delete" onClick={() => completeTask()}>
            ✔
          </span>
        </span>
      )}
    </div>
  );
}

export default Task;
