import React, { useState } from "react";
import axios from "axios";

function Registration({ setUser, toggleForm }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [checkPassword, setCheckPassword] = useState("");

  const handleRegistrationSubmit = (evt) => {
    evt.preventDefault();

    axios
      .post("http://localhost:3000/auth/register", { email, password })
      .then(({ data }) => {
        setUser(data.auth);
      })
      .catch(() => {
        setPassword("");
      });
  };

  return (
    <form className="Registration Form" onSubmit={handleRegistrationSubmit}>
      <h1>Registration</h1>

      <input id="email" type="email" placeholder="Email" value={email} onChange={({ target }) => setEmail(target.value)} />
      <input id="password" type="password" placeholder="Password" value={password} onChange={({ target }) => setPassword(target.value)} />
      <input id="checkPassword" type="password" placeholder="Repeat Password" value={checkPassword} onChange={({ target }) => setCheckPassword(target.value)} />

      <input type="submit" disabled={!email || !password || password !== checkPassword} />
    </form>
  );
}

export default Registration;
