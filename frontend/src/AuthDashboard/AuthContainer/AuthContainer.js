import React, { useState } from "react";
import Registration from "../Registration/Registration";
import Login from "../Login/Login";
import "./Container.css";

function AuthContainer({ setUser }) {
  const [registered, toggleForm] = useState(true);

  return (
    <div className="authContainer">
      {registered ? <Login setUser={setUser} /> : <Registration setUser={setUser} />}
      <button
        onClick={() => {
          toggleForm(!registered);
        }}
      >
        {registered ? "Register" : "Login"}
      </button>
    </div>
  );
}

export default AuthContainer;
