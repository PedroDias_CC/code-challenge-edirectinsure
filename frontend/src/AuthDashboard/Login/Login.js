import React, { useState } from "react";
import axios from "axios";
import "./Login";

function Login({ setUser }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLoginSubmit = (evt) => {
    evt.preventDefault();

    axios
      .post("http://localhost:3000/auth/login", { email, password })
      .then(({ data }) => {
        setUser(data.auth);
      })
      .catch(() => {
        setPassword("");
      });
  };

  return (
    <form className="loginForm" onSubmit={handleLoginSubmit}>
      <h1>Login</h1>

      <input id="email" type="email" placeholder="Email" value={email} onChange={({ target }) => setEmail(target.value)} />
      <input id="password" type="password" placeholder="Password" value={password} onChange={({ target }) => setPassword(target.value)} />

      <input type="submit" disabled={!email || !password} />
    </form>
  );
}

export default Login;
