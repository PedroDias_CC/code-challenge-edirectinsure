import React, { useState } from "react";
import "./App.css";
import AuthContainer from "./AuthDashboard/AuthContainer/AuthContainer";
import ProjectsContainer from "./ProjectsDashboard/ProjectsContainer/ProjectsContainer";

function App() {
  const [user, setUser] = useState(null);

  const view = user ? <ProjectsContainer user={user} /> : <AuthContainer setUser={setUser} />;

  return <div className="app">{view}</div>;
}

export default App;
