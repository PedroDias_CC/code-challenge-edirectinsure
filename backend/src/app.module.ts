import { Module } from '@nestjs/common';
import { ProjectsController } from './projects/projects.controller';
import { ProjectsService } from './projects/projects.service';
import { TasksService } from './tasks/tasks.service';
import { TasksController } from './tasks/tasks.controller';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { options } from './configs/typeormConfigs';
import { User } from './users/users.entity';
import { Project } from './projects/projects.entity';
import { Task } from './tasks/tasks.entity';
import { AuthService } from './auth/auth.service';

@Module({
  imports: [TypeOrmModule.forRoot(options), TypeOrmModule.forFeature([User, Project, Task])],
  controllers: [ProjectsController, TasksController, UsersController],
  providers: [ProjectsService, TasksService, UsersService, AuthService],
})
export class AppModule {}
