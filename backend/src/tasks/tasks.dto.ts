import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class TaskDto {
  @IsNumber()
  @IsNotEmpty()
  projectId: number;

  @IsString()
  @IsNotEmpty()
  todo: string;
}
