import { Controller, Headers, Post, HttpCode, Put, Delete, ParseIntPipe, Body, Param, Patch } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { TaskDto } from './tasks.dto';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private taskService: TasksService, private authService: AuthService) {}

  @Post()
  @HttpCode(204)
  async addTask(@Headers('authorization') userToken: string, @Body() task: TaskDto) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.taskService.addTask(userId, task);
  }

  @Put(':id')
  @HttpCode(204)
  async updateTask(@Headers('authorization') userToken: string, @Param('id', ParseIntPipe) id: number, @Body() task: TaskDto) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.taskService.updateTask(userId, id, task);
  }

  @Patch(':id')
  @HttpCode(204)
  async completeTask(@Headers('authorization') userToken: string, @Param('id', ParseIntPipe) id: number) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.taskService.completeTask(userId, id);
  }

  @Delete(':id')
  @HttpCode(204)
  async deleteProject(@Headers('authorization') userToken: string, @Param('id', ParseIntPipe) id: number) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.taskService.deleteTask(userId, id);
  }
}
