import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Task } from './tasks.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions } from 'typeorm';
import { Project } from '../projects/projects.entity';
import { TaskDto } from './tasks.dto';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private taskRepository: Repository<Task>,
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
  ) {}

  async addTask(userId: number, { todo, projectId }: TaskDto) {
    const project = await this.getProject(userId, projectId);
    return await this.taskRepository.insert({ todo, project });
  }
  async updateTask(userId: number, id: number, { todo }: TaskDto) {
    await this.checkOwnership(userId, id);
    return await this.taskRepository.update(id, { todo });
  }

  async completeTask(userId: number, id: number) {
    await this.checkOwnership(userId, id);
    return await this.taskRepository.update(id, { state: true });
  }

  async deleteTask(userId: number, id: number) {
    await this.checkOwnership(userId, id);
    return await this.taskRepository.delete(id);
  }

  private async getProject(userId: number, projectId: number) {
    const project = await this.projectRepository.findOne({ where: { id: projectId, user: { id: userId } } });
    if (!project) throw new UnauthorizedException();
    return project;
  }
  private async checkOwnership(userId: number, taskId: number) {
    const task = await this.taskRepository.findOne({ where: { id: taskId }, relations: ['project'] });
    if (!task || task.project.user.id !== userId) throw new UnauthorizedException();
  }
}
