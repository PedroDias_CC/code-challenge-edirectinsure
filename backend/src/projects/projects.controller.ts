import { Controller, Get, Post, Put, Delete, Headers, Param, ParseIntPipe, Body, HttpCode } from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectDto } from './projects.dto';
import { AuthService } from '../auth/auth.service';

@Controller('projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService, private authService: AuthService) {}
  @Get()
  async getProjects(@Headers('authorization') userToken: string) {
    const userId = this.authService.validateJWTToken(userToken);
    return await this.projectsService.getProjects(userId);
  }

  @Post()
  @HttpCode(204)
  async addProject(@Headers('authorization') userToken: string, @Body() project: ProjectDto) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.projectsService.addProject(userId, project);
  }

  @Put(':id')
  @HttpCode(204)
  async updateProject(@Headers('authorization') userToken: string, @Param('id', ParseIntPipe) id: number, @Body() project: ProjectDto) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.projectsService.updateProject(userId, id, project);
  }

  @Delete(':id')
  @HttpCode(204)
  async deleteProject(@Headers('authorization') userToken: string, @Param('id', ParseIntPipe) id: number) {
    const userId = this.authService.validateJWTToken(userToken);
    await this.projectsService.deleteProject(userId, id);
  }
}
