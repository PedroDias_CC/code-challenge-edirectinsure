import { IsString, IsNumber, ValidateIf, IsNotEmpty } from 'class-validator';

export class ProjectDto {
  @ValidateIf(dto => dto.id)
  @IsNumber()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;
}
