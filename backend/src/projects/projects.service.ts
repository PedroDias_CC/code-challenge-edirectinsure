import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from './projects.entity';
import { Repository } from 'typeorm';
import { User } from '../users/users.entity';
import { ProjectDto } from './projects.dto';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getProjects(userId: number) {
    return this.projectRepository.findAndCount({ where: { user: await this.getUser(userId) } });
  }
  async addProject(userId: number, { title }: ProjectDto) {
    return await this.projectRepository.insert({ title, user: await this.getUser(userId) });
  }
  async updateProject(userId: number, id: number, { title }: ProjectDto) {
    await this.checkOwnership(userId, id);
    return await this.projectRepository.update(id, { title });
  }
  async deleteProject(userId: number, id: number) {
    await this.checkOwnership(userId, id);
    return await this.projectRepository.delete(id);
  }

  private async getUser(userId: number) {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) throw new UnauthorizedException();
    return user;
  }

  private async checkOwnership(userId: number, projectId: number) {
    const project = await this.projectRepository.findOne({ where: { id: projectId, user: { id: userId } } });
    if (!project) throw new UnauthorizedException();
  }
}
