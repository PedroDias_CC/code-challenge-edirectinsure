import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './users.entity';
import { Repository } from 'typeorm';
import { UserDto } from './users.dto';
import { hash, compare } from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async register({ email, password }: UserDto) {
    password = await hash(password, 10);
    return await this.usersRepository.insert({ email, password });
  }
  async login({ email, password }: UserDto) {
    const user = await this.usersRepository.findOne({ where: { email }, select: ['id', 'password'] });
    if (user && (await compare(password, user.password))) return user;
    throw new UnauthorizedException();
  }
}
