import { Controller, Post, Body } from '@nestjs/common';
import { UserDto } from './users.dto';
import { sign } from 'jsonwebtoken';
import { UsersService } from './users.service';

@Controller('auth')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post('register')
  async register(@Body() userInfo: UserDto) {
    const insertResult = await this.usersService.register(userInfo);
    return { auth: this.generateToken(insertResult.identifiers[0].id, userInfo.email) };
  }

  @Post('login')
  async login(@Body() userInfo: UserDto) {
    const user = await this.usersService.login(userInfo);
    return { auth: this.generateToken(user.id, user.email) };
  }

  private generateToken(id: number, email: string) {
    return sign({ id, email }, process.env.JWT_SECRET);
  }
}
