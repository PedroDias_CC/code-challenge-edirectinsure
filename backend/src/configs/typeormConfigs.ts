import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Project } from '../projects/projects.entity';
import { Task } from '../tasks/tasks.entity';

export const options: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: 'postgres',
  entities: [User, Project, Task],
  synchronize: true,
  autoLoadEntities: true,
  logger: 'advanced-console',
};
