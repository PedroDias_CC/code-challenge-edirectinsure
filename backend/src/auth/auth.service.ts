import { Injectable, UnauthorizedException } from '@nestjs/common';
import { verify } from 'jsonwebtoken';

@Injectable()
export class AuthService {
  validateJWTToken(jwt: string) {
    if (!jwt) throw new UnauthorizedException();

    try {
      const payload = verify(jwt, process.env.JWT_SECRET);
      return payload['id'];
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
